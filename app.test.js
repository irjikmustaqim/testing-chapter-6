const axios = require('axios')

it('getAll', async () => {
    let response = await axios.get("http://localhost:3000/users/")
    expect(response.data.status).toBe('success')
});

it('get', async () => {
    let response = await axios.get("http://localhost:3000/users/2")
    expect(response.data.status).toBe('success')
});

it('post', async () => {
    let response = await axios(
        {
            method: 'post',
            url: "http://localhost:3000/users/",
            data: {
              username:"ucup",
              password:"hello",
              age:23,
              gender:"Laki - laki"
            }
          }
    )
    expect(response.data.status).toBe('success')
});

it('update', async () => {
    let response = await axios(
        {
            method: 'put',
            url: "http://localhost:3000/users/2",
            data: {
              username:"ucup",
              password:"hello",
            }
          }
    )
    expect(response.data.status).toBe('success')
});
it('delete', async () => {
    let response = await axios(
        {
            method: 'delete',
            url: "http://localhost:3000/users/2"
          }
    )
    expect(response.data.status).toBe('success')
});